
Pipeline status:
[![pipeline status](https://gitlab.com/BloedeBleidd/stm32-with-google-test-example-project/badges/develop/pipeline.svg)](https://gitlab.com/BloedeBleidd/stm32-with-google-test-example-project/-/commits/develop)
# CLI example project - STM32 GTest Doxygen CI

Purpose behind making this project was to implement and use good practices that are implemented in commercial embedded projects.<br> The whole project consists of multiple parts such as unit tests, software documentation and very useful continuous <br>integration and continuous delivery CI/CD processes which helped in the development process by unloading programmers from the burden <br>of executing repetitive tasks manually, such as formatting code or generating documentation.

Unit Testing supported by CI/CD approach was undertaken in order to fit into modern days standards of code manufacturing. <br>

For those that are curious, configuration for gitlab's pipeline was defined in .gitlab-ci.yml file. <br> 
Considered file also includes precompiled docker image supporting pipeline exucution. <br>
https://hub.docker.com/r/bloedebleidd/embedded-docker-gitlab-runner/tags


# Block diagram

![](doxygen/resources/blockDiagram.png)

There are 3 tasks responsible for various processes in the project:

1.  The least meaningful task is responsible for blinking status LED. It is used primarily for visual monitoring of the device. <br>The continuous blinking with consistent frequency means everything works correctly. If the LED doesn’t change its state, something is wrong.
    
2.  It is the main task of the project as it consumes the most significant of both cpu’s time and resources.<br> It manages the overall communication between user and microcontroller. It parses the strings entered by the user and interprets them.<br> It also executes actions that are assigned to the defined commands.
    
3.  This task’s purpose is to periodically restart the hardware watchdog. If a device enters into a hard fault, the watchdog will restart the device. <br>It happens when its counter reaches threshold value.

#  Main features

The main purpose of the project is to execute gpio actions in a manner specified by the user with commands that are being entered on his end.<br> Below is a list of commands that user can enter with their short description.

  

1.  help - List of all available commands
    
2.  time - System time in [ms]
    
3.  gpio \<number>.\<action>, where number is specified on the pcb board next to every pin

Action can be chosen from one of the below:

-   hz - input High Impedance
    
-   pu - input with Pull-Up
    
-   pd - input with Pull-Down
    
-   pp - output as push-Pull
    
-   od - output as Open-Drain
    
-   odpu - output as Open-Drain with Pull-Up
    
-   get - read pin state
    
-   1/0 - set pin state
    

 
Example: gpio b10.odpu - GPIO B10 as Open-Drain with Pull-Up.<br>

Project was thought of in such a way that it can be further extended by adding more commands to it. <br>All one has to do is specify the command's name, its description and action tied to it by a callback function. <br>It supports dynamic management of currently used commands because of its modular nature and provides API documentation. <br>


# Project's components

In the development process following assumptions were proposed in order to fulfil the main goal of the project:<br>


FreeRTOS - It’s open source licence allows for easy implementation and the fact that it’s an operating system, enables tighter control over the tasks.<br>

  

Serial - UART was used as a communication protocol between user and microcontroller. Interrupt handler was used for sending and receiving data. <br>It will work at 19200 baud/s. The frame will contain 8 bits of data, no parity bits and 1 stop bit. Pins PB7/PB6 serve as RX/TX accordingly<br>

  

CLI - Command line interface which works in a similar fashion to the one that POSIX based operating systems use but in much simpler form.<br>

  

uC - STM32F103C8T6 was used as a working platform. It’s relatively rich in resources for this particular project as it has UART and more than enough memory for stack and heap. <br>It’s also clocked at 72MHz.<br>

  

UT - Google test (GTEST) is used as a testing framework. It provides descriptive documentation and there are also lots of exemplary projects using it that can be used as a reference point. <br>One does not have to limit its implementation ideas to smaller frameworks, instead GTEST can be used as can be seen in huge software applications.<br> Configured unit tests framework is present in /ut folder along with example ring buffer test. <br>

  

Documentation - Doxygen commenting conventions will be applied in code to enable documentation to be created automatically. <br>Doxygen allows you to create software documentation directly from the code itself. <br>One of the advantages is the automatic creation of dependency and call diagrams <br>

Link to the newest documentation generated from develop branch using CI pipeline:<br>
https://gitlab.com/BloedeBleidd/stm32-with-google-test-example-project/-/jobs/artifacts/develop/file/public/index.html?job=documentation

# Hardware

![Board - STM32F103 (bluepill)](doxygen/resources/blupilll.png)
<br>


![STM32 CUBE MX generated pinout](doxygen/resources/ucPinout.png)
<br>

![Microcontroller info table](doxygen/resources/ucInfo.png)
<br>

## IDE/Compiler

Toolchain / IDE : STM32CubeIDE V1.6.1<br>
Firmware Package: FW_F1 V1.8.3