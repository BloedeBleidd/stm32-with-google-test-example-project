/**
 * \file			testTemplate.cpp
 * \brief			Template UT file
 *
 * \dir             testTemplate
 * \brief           Template UT
 *
 * \date			Mar 15, 2021
 * \author			Piotr Zmuda - piotr.zmuda.dev@gmail.com
 * \version			v1.0
 */

#include <gtest/gtest.h>
#include "../../../core/code/template/template.hpp"

class TemplateTests : public testing::Test {
public:
    // some data

    virtual void SetUp() override
    {
        // some set-up stuff
    }

    virtual void TearDown() override
    {
        // some tear-down stuff
    }
};

TEST(TemplateTests, dummyCheck)
{
    EXPECT_TRUE(true);
}

TEST(TemplateTests, zeroCheck){
    EXPECT_EQ(0, templateGetZero());
}
