/**
 * \file			testRingBuffer.cpp
 * \brief			Tests for ring buffer <char>
 *
 * \dir             testRingBuffer
 * \brief           Collection of tests for ring buffer
 *
 * \date			Apr 30, 2021
 * \author			Michal Mlodecki
 * \version			v1.0
 */

#include <gtest/gtest.h>
#include "../../../core/code/ringBuffer/ringBuffer.hpp"

typedef RingBuffer<char> myBuffer;

class RingBufferTests : public testing::Test
{
public:
    // some data
	const int bufferSize = 1000;
	
	char* buffer = nullptr;
	myBuffer *ringBuffer = nullptr;

    virtual void SetUp() override {
        // some set-up stuff
    	buffer = new char[bufferSize];
    	ringBuffer = new myBuffer(buffer, bufferSize);
    }

    virtual void TearDown() override {
        // some tear-down stuff
    	delete[] buffer;
    	delete ringBuffer;
    }
};

TEST_F(RingBufferTests, checkSizeAfterInitRingBuffer) {

	EXPECT_EQ(ringBuffer->capacity(), bufferSize);
}

TEST_F(RingBufferTests, checkEmptyAfterInitRingBuffer) {

	EXPECT_TRUE(ringBuffer->isEmpty());
}

TEST_F(RingBufferTests, checkLengthAfterInitRingBuffer) {


	EXPECT_EQ(ringBuffer->length(), 0);
}

TEST_F(RingBufferTests, checkFullRingBuffer)
{
	for(int i = 0; i < bufferSize; i++)
	{
		ringBuffer->put(i%100);
	}

	EXPECT_TRUE(ringBuffer->isFull());
}

TEST_F(RingBufferTests, checkLengthRingBuffer)
{
	const int size = 100;
	for(int i = 0; i < 100; i++)
	{
		ringBuffer->put(10);
	}

	EXPECT_EQ(ringBuffer->length(), size);
}

TEST_F(RingBufferTests, checkClearRingBuffer)
{
	ringBuffer->clear();
	EXPECT_TRUE(ringBuffer->isEmpty());
	EXPECT_FALSE(ringBuffer->isFull());
	EXPECT_EQ(ringBuffer->length(), 0);
}

TEST_F(RingBufferTests, checkPutRingBuffer)
{
	for(int i = 0; i < bufferSize; i++)
	{
		EXPECT_TRUE(ringBuffer->put(i%bufferSize));
	}
}

TEST_F(RingBufferTests, checkPutOverflowRingBuffer)
{
	for(int i = 0; i < bufferSize; i++)
	{
		ringBuffer->put(i%bufferSize);
	}

	for(int i = 0; i < bufferSize+1; i++)
	{
		EXPECT_FALSE(ringBuffer->put(i%bufferSize));
	}
}

TEST_F(RingBufferTests, checkGetRingBuffer)
{
	for(int i = 0; i < bufferSize; i++)
	{
		char tmp;
		EXPECT_TRUE(ringBuffer->put(i%bufferSize));
	}
}

TEST_F(RingBufferTests, checkGetEmptyRingBuffer)
{
	for(int i = 0; i < bufferSize+1; i++)
	{
		char tmp;
		EXPECT_FALSE(ringBuffer->get(&tmp));
	}
}

TEST_F(RingBufferTests, checkLengthUpdateRingBuffer)
{
	for(int i = 0; i < bufferSize; i++)
	{
		EXPECT_EQ(ringBuffer->length(), i);
		ringBuffer->put(i%10);
	}
}
