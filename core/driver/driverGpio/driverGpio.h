/**
 * \file			driverGpio.h
 * \brief			GPIO Driver API
 *
 * \dir             driverGpio
 * \brief           GPIO Driver
 *
 * \date			Apr 29, 2021
 * \author			Piotr Zmuda - piotr.zmuda.dev@gmail.com
 * \version			v1.0
 */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>

/**
 * \brief	All available pins on the board with their corresponding numbers and ports
 */
typedef enum {
    DRIVER_GPIO_PIN_B11 = 0,
    DRIVER_GPIO_PIN_B10,
    DRIVER_GPIO_PIN_B1,
    DRIVER_GPIO_PIN_B0,
    DRIVER_GPIO_PIN_A7,
    DRIVER_GPIO_PIN_A6,
    DRIVER_GPIO_PIN_A5,
    DRIVER_GPIO_PIN_A4,
    DRIVER_GPIO_PIN_A3,
    DRIVER_GPIO_PIN_A2,
    DRIVER_GPIO_PIN_A1,
    DRIVER_GPIO_PIN_A0,
    DRIVER_GPIO_PIN_C15,
    DRIVER_GPIO_PIN_C14,
    DRIVER_GPIO_PIN_B9,
    DRIVER_GPIO_PIN_B8,
    DRIVER_GPIO_PIN_B5,
    DRIVER_GPIO_PIN_B4,
    DRIVER_GPIO_PIN_B3,
    DRIVER_GPIO_PIN_A15,
    DRIVER_GPIO_PIN_A12,
    DRIVER_GPIO_PIN_A11,
    DRIVER_GPIO_PIN_A10,
    DRIVER_GPIO_PIN_A9,
    DRIVER_GPIO_PIN_A8,
    DRIVER_GPIO_PIN_B15,
    DRIVER_GPIO_PIN_B14,
    DRIVER_GPIO_PIN_B13,
    DRIVER_GPIO_PIN_B12,
    DRIVER_GPIO_PIN_AMOUNT
} DriverGpioPin;

/**
 * \brief	Possible modes of operation for a given pin
 */
typedef enum {
    DRIVER_GPIO_MODE_INPUT_HIGH_Z = 0,
    DRIVER_GPIO_MODE_INPUT_PULL_UP,
    DRIVER_GPIO_MODE_INPUT_PULL_DOWN,
    DRIVER_GPIO_MODE_OUTPUT_PUSH_PULL,
    DRIVER_GPIO_MODE_OUTPUT_OPEN_DRAIN,
    DRIVER_GPIO_MODE_OUTPUT_OPEN_DRAIN_PULL_UP,
    DRIVER_GPIO_MODE_UNSUPPORTED
} DriverGpioMode;

/**
 * Set up gpio and it's pin to a specified mode
 * @param pin gpio pin enum
 * @param mode gpio mode enum
 * @return none
 */
void driverGpioModeSet(DriverGpioPin pin, DriverGpioMode mode);

/**
 * Set gpio pin output to high or low
 * @param pin gpio pin enum
 * @param state gpio pin output logical value
 * @return none
 */
void driverGpioSet(DriverGpioPin pin, bool state);

/**
 * Read pin's logical value
 * @param pin gpio pin
 * @return true if logical read value is 1, false if it's 0
 */
bool driverGpioGet(DriverGpioPin pin);

#ifdef __cplusplus
}
#endif
