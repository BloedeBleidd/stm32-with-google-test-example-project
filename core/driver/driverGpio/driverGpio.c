/**
 * \file			driverGpio.c
 * \brief			GPIO Driver Layer for pins available from CLI
 *
 * \date			Apr 29, 2021
 * \author			Piotr Zmuda - piotr.zmuda.dev@gmail.com
 * \version			v1.0
 */

#include "driverGpio.h"
#include "main.h"
#include "stm32f1xx_hal.h"

typedef struct {
    GPIO_TypeDef* gpio;
    uint16_t pin;
} GpioPin;

typedef uint32_t GpioMode;

typedef uint32_t GpioPull;

/**
 * LUT filled with structs describing GPIO port and pin
 */
static const GpioPin GPIO_PIN[DRIVER_GPIO_PIN_AMOUNT] = {
    {PB11_GPIO_Port, PB11_Pin}, {PB10_GPIO_Port, PB10_Pin}, {PB1_GPIO_Port, PB1_Pin},
    {PB0_GPIO_Port, PB0_Pin},   {PA7_GPIO_Port, PA7_Pin},   {PA6_GPIO_Port, PA6_Pin},
    {PA5_GPIO_Port, PA5_Pin},   {PA4_GPIO_Port, PA4_Pin},   {PA3_GPIO_Port, PA3_Pin},
    {PA2_GPIO_Port, PA2_Pin},   {PA1_GPIO_Port, PA1_Pin},   {PA0_GPIO_Port, PA0_Pin},
    {PC15_GPIO_Port, PC15_Pin}, {PC14_GPIO_Port, PC14_Pin}, {PB9_GPIO_Port, PB9_Pin},
    {PB8_GPIO_Port, PB8_Pin},   {PB5_GPIO_Port, PB5_Pin},   {PB4_GPIO_Port, PB4_Pin},
    {PB3_GPIO_Port, PB3_Pin},   {PA15_GPIO_Port, PA15_Pin}, {PA12_GPIO_Port, PA12_Pin},
    {PA11_GPIO_Port, PA11_Pin}, {PA10_GPIO_Port, PA10_Pin}, {PA9_GPIO_Port, PA9_Pin},
    {PA8_GPIO_Port, PA8_Pin},   {PB15_GPIO_Port, PB15_Pin}, {PB14_GPIO_Port, PB14_Pin},
    {PB13_GPIO_Port, PB13_Pin}, {PB12_GPIO_Port, PB12_Pin}};

/**
 * \brief   LUT describing pin's modes of operation
 */
static const GpioMode GPIO_MODE[DRIVER_GPIO_MODE_UNSUPPORTED] = {
    GPIO_MODE_INPUT,     GPIO_MODE_INPUT,     GPIO_MODE_INPUT,
    GPIO_MODE_OUTPUT_PP, GPIO_MODE_OUTPUT_OD, GPIO_MODE_OUTPUT_OD};

/**
 * \brief   LUT describing pin's available pull configurations
 */
static const GpioPull GPIO_PULL[DRIVER_GPIO_MODE_UNSUPPORTED] = {
    GPIO_NOPULL, GPIO_PULLUP, GPIO_PULLDOWN, GPIO_NOPULL, GPIO_NOPULL, GPIO_PULLUP};

void driverGpioModeSet(DriverGpioPin pin, DriverGpioMode mode)
{
    ASSERT(pin < DRIVER_GPIO_PIN_AMOUNT);
    ASSERT(mode < DRIVER_GPIO_MODE_UNSUPPORTED);

    GPIO_InitTypeDef gpio;
    gpio.Pin = GPIO_PIN[pin].pin;
    gpio.Mode = GPIO_MODE[mode];
    gpio.Pull = GPIO_PULL[mode];
    gpio.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(GPIO_PIN[pin].gpio, &gpio);
}

void driverGpioSet(DriverGpioPin pin, bool state)
{
    ASSERT(pin < DRIVER_GPIO_PIN_AMOUNT);

    HAL_GPIO_WritePin(GPIO_PIN[pin].gpio, GPIO_PIN[pin].pin, (state) ? GPIO_PIN_SET : GPIO_PIN_RESET);
}

bool driverGpioGet(DriverGpioPin pin)
{
    ASSERT(pin < DRIVER_GPIO_PIN_AMOUNT);

    return GPIO_PIN_SET == HAL_GPIO_ReadPin(GPIO_PIN[pin].gpio, GPIO_PIN[pin].pin);
}
