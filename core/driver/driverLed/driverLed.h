/**
 * \file			driverLed.h
 * \brief			Status LED Driver API
 *
 * \dir             driverLed
 * \brief           Status LED Driver
 *
 * \date			Mar 16, 2021
 * \author			Piotr Zmuda - piotr.zmuda.dev@gmail.com
 * \version			v1.0
 */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Turn on LED. LED info is specified in main header
 * @param none
 * return none
 */
void driverLedOn(void);

/**
 * Turn off LED. LED info is specified in main header
 * @param none
 * return none
 */
void driverLedOff(void);

/**
 * Toogle LED's state. LED info is specified in main header
 * @param none
 * return none
 */
void driverLedToggle(void);

#ifdef __cplusplus
}
#endif
