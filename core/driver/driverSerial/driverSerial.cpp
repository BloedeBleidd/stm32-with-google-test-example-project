/**
 * \file			driverSerial.cpp
 * \brief			Driver dealing with receiving and transporting ASCII characters through the UART port
 *
 * \date			Mar 16, 2021
 * \author			Piotr Zmuda - piotr.zmuda.dev@gmail.com
 * \version			v1.0
 */

#include "driverSerial.hpp"
#include "ringBuffer/ringBuffer.hpp"
#include "stm32_hal_legacy.h"
#include "usart.h"

#define USARTx (USART1)

/**
 * \brief Template class ring buffer is used in this module with a specialization as char for the ASCII serial interface
 */
typedef RingBuffer<char> Buffer;

static Buffer* bufferIn = nullptr;  //!< Static definition for ring buffer struct describing input data buffer
static Buffer* bufferOut = nullptr;  //!< Static definition for ring buffer struct describing output data buffer

static char bufferDataIn[256];   //!< Static definition for ring buffer's input data buffer
static char bufferDataOut[512];  //!< Static definition for ring buffer's output data buffer

void driverSerialInit(void)
{
    bufferIn = new Buffer(bufferDataIn, sizeof(bufferDataIn));
    bufferOut = new Buffer(bufferDataOut, sizeof(bufferDataOut));

    __HAL_UART_ENABLE_IT(&huart1, UART_IT_RXNE);
}

bool driverSerialWriteChar(char c)
{
    bool result = bufferOut->put(c);

    if (result) {
        __HAL_UART_ENABLE_IT(&huart1, UART_IT_TXE);
    }

    return result;
}

bool driverSerialWriteString(const char* str)
{
    for (size_t i = 0; *str != '\0'; i++, str++) {
        if (!driverSerialWriteChar(*str)) {
            return false;
        }
    }

    return true;
}

bool driverSerialReadChar(char* c)
{
    char data = '\0';

    if (!bufferIn->get(&data)) {
        return false;
    }

    *c = data;

    return true;
}

size_t driverSerialReadString(char* str, size_t size)
{
    size_t i = 0;

    for (; i < size; i++, str++) {
        if (!driverSerialReadChar(str)) {
            break;
        }
    }

    return i;
}

void driverSerialInterruptHandler(void)
{
    if (__HAL_UART_GET_FLAG(&huart1, UART_FLAG_RXNE)) {
        if (__HAL_UART_GET_IT_SOURCE(&huart1, UART_IT_RXNE)) {
            bufferIn->put(USARTx->DR);
        }
    }

    if (__HAL_UART_GET_FLAG(&huart1, UART_FLAG_TXE)) {
        if (__HAL_UART_GET_IT_SOURCE(&huart1, UART_IT_TXE)) {
            char c;

            if (bufferOut->get(&c)) {
                USARTx->DR = c;
            }

            if (bufferOut->isEmpty()) {
                __HAL_UART_DISABLE_IT(&huart1, UART_IT_TXE);
            }
        }
    }
}
