/**
 * \file			driverSerial.hpp
 * \brief			Serial Driver API
 *
 * \dir             driverSerial
 * \brief           Serial communication driver
 *
 * \date			Mar 16, 2021
 * \author			Piotr Zmuda - piotr.zmuda.dev@gmail.com
 * \version			v1.0
 */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <stddef.h>

/**
 * Enable RXNE interrupt. Heap space is reserved for ring buffer input and output objects.
 * From this point on, the data is already received and written to the input buffer
 * @param none
 * @return none
 */
void driverSerialInit(void);

/**
 * Put character into output ring buffer
 * @param c character to place into the ring buffer
 * @return true if character was sent successfully, false it was not
 */
bool driverSerialWriteChar(char c);

/**
 * Put string into output ring buffer
 * @param str string to place into the ring buffer
 * @return true if string was sent successfully, false it was not
 */
bool driverSerialWriteString(const char* str);

/**
 * Read input ring buffer and return character from the buffer
 * @param c char variable to place the read character in
 * @return true if character was read successfully, false it was not
 */
bool driverSerialReadChar(char* c);

/**
 * Read string from the input ring buffer
 * @param str storage for successfully read characters
 * @param size determines how many characters to read
 * @return amount of characters that were successfully read
 */
size_t driverSerialReadString(char* str, size_t size);

/**
 * Interrupt handler responsible for reading or writing characters to the input/output ring buffers
 * @param none
 * @return none
 */
void driverSerialInterruptHandler(void);

#ifdef __cplusplus
}
#endif
