/**
 * \file			app.cpp
 * \brief			The highest main project file that contains RTOS tasks
 *
 * \date			Mar 16, 2021
 * \author			Piotr Zmuda - piotr.zmuda.dev@gmail.com
 * \version			v1.0
 */

#include "app.hpp"
#include "FreeRTOS.h"
#include "cli/cli.h"
#include "commands/commands.h"
#include "led/led.h"
#include "main.h"
#include "task.h"
#include "watchdog/watchdog.h"

/**
 * Task which resets the watchdog every single second is used to reset the device after the suspension of the RTOS
 * @param params not used, implemented to maintain compatibility for freertos task api
 * @return none
 */
static void taskWatchdog(void* params);

/**
 * Task of blinking LED with 1 second period used to indicate that the system is working properly
 * @param params not used, implemented to maintain compatibility for freertos task api
 * @return none
 */
static void taskLed(void* params);

/**
 * Task of the CLI adding commands to the list and processing them accordingly.
 * Proceeds initialization of Serial hardware and ring buffers at the beginning.
 * This is the main task of the project and consumes the most CPU resources
 * @param params not used, implemented to maintain compatibility for freertos task api
 * @return none
 */
static void taskCli(void* params);

void appInit(void)
{
    ASSERT(pdPASS == xTaskCreate(taskWatchdog, "Watchdog", 64, NULL, 1, NULL));
    ASSERT(pdPASS == xTaskCreate(taskLed, "status led", 64, NULL, 1, NULL));
    ASSERT(pdPASS == xTaskCreate(taskCli, "CLI", 512, NULL, 1, NULL));
}

static void taskWatchdog(void* params)
{
    for (;;) {
        watchdogRefresh();
        vTaskDelay(1000);
    }
}

static void taskLed(void* params)
{
    TickType_t time = xTaskGetTickCount();

    for (;;) {
        vTaskDelayUntil(&time, 500);
        ledToggle();
    }
}

static void taskCli(void* params)
{
    cliInit();

    cliAddCommand(&CLI_HELP);
    cliAddCommand(&CLI_TIME);
    cliAddCommand(&CLI_GPIO);

    while (1) {
        cliProcess();
        vTaskDelay(1);
    }
}

/**
 * Handler called by FreeRTOS in the IDLE task used to put the CPU to sleep
 * @param params none
 * @return none
 */
extern "C" void vApplicationIdleHook(void)
{
    __WFI();
}
