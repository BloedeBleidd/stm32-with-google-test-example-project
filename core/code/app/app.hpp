/**
 * \file			app.hpp
 * \brief			The highest main project header file containing application initialization called after system startup
 *
 * \dir             app
 * \brief           Top level application
 *
 * \date			Mar 16, 2021
 * \author			Piotr Zmuda - piotr.zmuda.dev@gmail.com
 * \version			v1.0
 */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Ensures proper initialisation of the tasks.
 * It is the main handler intermediary between an auto-generated c file and c++ code.
 * Prevents the main.c file extension from being automatically changed each time project files are generated in CubeMX
 * @param none
 * @return none
 */
void appInit(void);

#ifdef __cplusplus
}
#endif
