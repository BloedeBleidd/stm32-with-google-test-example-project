/**
 * \file			led.h
 * \brief			Status LED API
 *
 * \dir             led
 * \brief           Status LED support
 *
 * \date			Mar 16, 2021
 * \author			Piotr Zmuda - piotr.zmuda.dev@gmail.com
 * \version			v1.0
 */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Turn on status LED
 * @param none
 * @return none
 */
void ledOn(void);

/**
 * Turn of status LED
 * @param none
 * @return none
 */
void ledOff(void);

/**
 * Toogle status LED
 * @param none
 * @return none
 */
void ledToggle(void);

#ifdef __cplusplus
}
#endif
