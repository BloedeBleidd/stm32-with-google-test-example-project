/**
 * \file			watchdog.h
 * \brief			Watchdog API
 *
 * \dir             watchdog
 * \brief           Watchdog that takes care of resetting the device after a failure
 *
 * \date			Apr 29, 2021
 * \author			Piotr Zmuda - piotr.zmuda.dev@gmail.com
 * \version			v1.0
 */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Watchdog timer refresh using watchdog driver layer. Works only when DEBUG macro is not specified
 * @param none
 * @param none
 */
void watchdogRefresh(void);

#ifdef __cplusplus
}
#endif
