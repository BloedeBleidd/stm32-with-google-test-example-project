/**
 * \file			template.hpp
 * \brief			Empty file for UT API
 *
 * \dir             template
 * \brief           Source for template UT's
 *
 * \date			Mar 15, 2021
 * \author			Piotr Zmuda - piotr.zmuda.dev@gmail.com
 * \version			v1.0
 */

#pragma once

#include <cstdint>

int templateGetZero(void);
