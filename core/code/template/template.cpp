/**
 * \file			template.cpp
 * \brief			Empty file for UT process without real testing
 *
 * \date			Mar 15, 2021
 * \author			Piotr Zmuda - piotr.zmuda.dev@gmail.com
 * \version			v1.0
 */

#include "template.hpp"

int templateGetZero(void)
{
    return 0;
}
