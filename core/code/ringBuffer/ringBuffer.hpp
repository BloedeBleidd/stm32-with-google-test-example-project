/**
 * \file			ringBuffer.hpp
 * \brief			The template class of the circular buffer module
 *
 * \dir             ringBuffer
 * \brief           Ring buffer library
 *
 * \date			Mar 16, 2021
 * \author			Piotr Zmuda - piotr.zmuda.dev@gmail.com
 * \version			v1.0
 */

#pragma once

#ifdef UNIT_TEST
/**
 * \brief	This config ensures that the hardware assert described in the main.h file is separated from the one used in unit tests
 */
#define ASSERT(expr) \
    if (!(expr)) {   \
        for (;;) {   \
        }            \
    }
#else
#include "main.h"
#endif

#include <stddef.h>

/**
 * \brief   General purpose circular buffer
 * \tparam T single buffer element
 */
template <class T>
class RingBuffer {
public:
    /**
     * Constructor
     * @param buffer data storage
     * @param size number of elements
     */
    RingBuffer(T* buffer, size_t size);

    /**
     * Resets both beginning and end pointers of buffer without clearing the buffer
     * @param none
     * @return none
     */
    void clear();

    /**
     * Checks whether there are any elements in the buffer
     * @param none
     * @return returns true when buffer contains no elements
     */
    bool isEmpty() const;

    /**
     * Checks whether there is any space left in the buffer
     * @param none
     * @return returns true when the buffer contains maximum number of elements
     */
    bool isFull() const;

    /**
     * Determines how many elements are currently in the buffer
     * @param none
     * @return amount of elements
     */
    size_t length() const;

    /**
     * Returns total number of elements that the buffer can store
     * @param none
     * @return maximum number of elements
     */
    size_t capacity() const;

    /**
     * Enters new data to the buffer
     * @param data element to store
     * @return true : data was entered successfully, false : no space for the element
     */
    bool put(T data);

    /**
     * Gives the element stored in the buffer
     * @param data element to be given
     * @return true : data was given successfully, false : data not given and the buffer is empty
     */
    bool get(T* data);

private:
    T* buffer;
    size_t max;
    size_t head;
    size_t tail;
    bool full;
};

template <class T>
RingBuffer<T>::RingBuffer(T* buffer, size_t size)
{
    ASSERT(buffer);

    this->buffer = buffer;
    max = size;
    clear();

    ASSERT(isEmpty());
}

template <class T>
void RingBuffer<T>::clear()
{
    head = 0;
    tail = 0;
    full = false;
}

template <class T>
bool RingBuffer<T>::isEmpty() const
{
    return !full && (head == tail);
}

template <class T>
bool RingBuffer<T>::isFull() const
{
    return full;
}

template <class T>
size_t RingBuffer<T>::length() const
{
    size_t size = max;

    if (!full) {
        if (head >= tail) {
            size = head - tail;
        }
        else {
            size = max + head - tail;
        }
    }

    return size;
}

template <class T>
size_t RingBuffer<T>::capacity() const
{
    return max;
}

template <class T>
bool RingBuffer<T>::put(T data)
{
    if (!full) {
        buffer[head] = data;

        if (full) {
            tail = (tail + 1) % max;
        }

        head = (head + 1) % max;
        full = (head == tail);

        return true;
    }

    return false;
}

template <class T>
bool RingBuffer<T>::get(T* data)
{
    ASSERT(data);

    if (!isEmpty()) {
        *data = buffer[tail];
        full = false;
        tail = (tail + 1) % max;

        return true;
    }

    return false;
}
