/**
 * \file			gpioParser.cpp
 * \brief			Gpio command parser that determines the execution of a specific action
 *
 * \date			Apr 29, 2021
 * \author			Piotr Zmuda - piotr.zmuda.dev@gmail.com
 * \version			v1.0
 */

#include "gpioParser.h"
#include <stddef.h>
#include <string.h>

/**
 * \brief   LUT with strings defining all pins that are not used by serial or SWD debugger/programmer
 */
const static char* GPIO_PARSER_PIN_LUT[GPIO_PARSER_PIN_UNSUPPORTED] = {
    {"b11"}, {"b10"}, {"b1"},  {"b0"},  {"a7"}, {"a6"},  {"a5"},  {"a4"},  {"a3"}, {"a2"},
    {"a1"},  {"a0"},  {"c15"}, {"c14"}, {"b9"}, {"b8"},  {"b5"},  {"b4"},  {"b3"}, {"a15"},
    {"a12"}, {"a11"}, {"a10"}, {"a9"},  {"a8"}, {"b15"}, {"b14"}, {"b13"}, {"b12"}};

/**
 * \brief   LUT with strings defining all supported actions towards specified pin
 */
const static char* GPIO_PARSER_ACTION_LUT[GPIO_PARSER_ACTION_UNSUPPORTED] = {
    {"hz"},                        //!< High impedance input
    {"pu"},                        //!< Input with the Pull-Up resistor
    {"pd"},                        //!< Input with the Pull-Up resistor
    {"pp"},                        //!< Push-Pull output mode
    {"od"},                        //!< Open-Drain output mode
    {"odpu"},                      //!< Open-Drain with Pull-Up resistor
    {"get"},                       //!< Read value of the pin
    {GPIO_PARSER_PIN_TRUE_VALUE},  //!< Set pin to high state
    {GPIO_PARSER_PIN_FALSE_VALUE}  //!< Set pin to low state
};

GpioParserPin gpioParsePin(const char* command)
{
    char pinStr[5];
    size_t i = 0;

    for (; (i < 3) && (*command); i++, command++) {
        if (*command == GPIO_PARSER_SEPARATOR_CHAR) {
            break;
        }

        pinStr[i] = *command;
    }

    pinStr[i] = '\0';

    for (size_t i = 0; i < GPIO_PARSER_PIN_UNSUPPORTED; i++) {
        if (0 == strcmp(GPIO_PARSER_PIN_LUT[i], pinStr)) {
            return GpioParserPin(i);
        }
    }

    return GPIO_PARSER_PIN_UNSUPPORTED;
}

GpioParserAction gpioParseAction(const char* command)
{
    for (size_t i = 0; i < GPIO_PARSER_ACTION_UNSUPPORTED; i++) {
        if (0 == strcmp(GPIO_PARSER_ACTION_LUT[i], command)) {
            return GpioParserAction(i);
        }
    }

    return GPIO_PARSER_ACTION_UNSUPPORTED;
}
