/**
 * \file			gpioParser.h
 * \brief			Gpio command parser API
 *
 * \dir             gpioParser
 * \brief           Gpio command parser
 *
 * \date			Apr 29, 2021
 * \author			Piotr Zmuda - piotr.zmuda.dev@gmail.com
 * \version			v1.0
 */

#pragma once

#include "driverGpio/driverGpio.h"

/**
 * \brief   A constant character recognized by the parser as a separator between number and action
 */
#define GPIO_PARSER_SEPARATOR_CHAR ('.')

/**
 * \brief   A constant character recognized by the parser as a 0 value
 */
#define GPIO_PARSER_PIN_FALSE_VALUE ("0")

/**
 * \brief   A constant character recognized by the parser as a 1 value
 */
#define GPIO_PARSER_PIN_TRUE_VALUE ("1")

/**
 * \brief   List of pins supported by gpio CLI
 */
typedef enum {
    GPIO_PARSER_PIN_B11 = 0,
    GPIO_PARSER_PIN_B10,
    GPIO_PARSER_PIN_B1,
    GPIO_PARSER_PIN_B0,
    GPIO_PARSER_PIN_A7,
    GPIO_PARSER_PIN_A6,
    GPIO_PARSER_PIN_A5,
    GPIO_PARSER_PIN_A4,
    GPIO_PARSER_PIN_A3,
    GPIO_PARSER_PIN_A2,
    GPIO_PARSER_PIN_A1,
    GPIO_PARSER_PIN_A0,
    GPIO_PARSER_PIN_C15,
    GPIO_PARSER_PIN_C14,
    GPIO_PARSER_PIN_B9,
    GPIO_PARSER_PIN_B8,
    GPIO_PARSER_PIN_B5,
    GPIO_PARSER_PIN_B4,
    GPIO_PARSER_PIN_B3,
    GPIO_PARSER_PIN_A15,
    GPIO_PARSER_PIN_A12,
    GPIO_PARSER_PIN_A11,
    GPIO_PARSER_PIN_A10,
    GPIO_PARSER_PIN_A9,
    GPIO_PARSER_PIN_A8,
    GPIO_PARSER_PIN_B15,
    GPIO_PARSER_PIN_B14,
    GPIO_PARSER_PIN_B13,
    GPIO_PARSER_PIN_B12,
    GPIO_PARSER_PIN_UNSUPPORTED
} GpioParserPin;

/**
 * \brief   All supported actions towards specified pin
 */
typedef enum {
    GPIO_PARSER_ACTION_INPUT_HIGH_Z = 0,
    GPIO_PARSER_ACTION_INPUT_PULL_UP,
    GPIO_PARSER_ACTION_INPUT_PULL_DOWN,
    GPIO_PARSER_ACTION_OUTPUT_PUSH_PULL,
    GPIO_PARSER_ACTION_OUTPUT_OPEN_DRAIN,
    GPIO_PARSER_ACTION_OUTPUT_OPEN_DRAIN_PULL_UP,
    GPIO_PARSER_ACTION_READ,
    GPIO_PARSER_ACTION_WRITE_TRUE,
    GPIO_PARSER_ACTION_WRITE_FALSE,
    GPIO_PARSER_ACTION_UNSUPPORTED
} GpioParserAction;

/**
 * Function responsible for parsing input data and associating it with pin specified in the argument
 * @param command string specifing uC's pin on which futrther actions will be performed
 * @return gpio pin enumerator associated with the argument that was entered
 */
GpioParserPin gpioParsePin(const char* command);

/**
 * Function responsible for parsing input data and associating it with executing specified action towards the uC's pin
 * @param command string specifing what action to take regarding uC's pin
 * @return gpio action enumerator associated with the argument that was entered
 */
GpioParserAction gpioParseAction(const char* command);
