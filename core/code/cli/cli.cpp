/**
 * \file			cli.cpp
 * \brief			CLI management layer dealing with the storage and parsing of commands
 *
 * \date			Apr 29, 2021
 * \author			Piotr Zmuda - piotr.zmuda.dev@gmail.com
 * \version			v1.0
 */

#include "cli.h"
#include "driverSerial/driverSerial.hpp"
#include <ctype.h>
#include <string.h>

static CliCommand* head = NULL;            //!< beginning of the command list
static CliCommand* currentCommand = NULL;  //!< pointer to the location of the newest added command

static char commandBuffer[128];  //!< buffer for commands

static uint8_t afterSpaceLoc;  //!< space index location

/**
 * Identifies command by the name
 * @param command string that represents the command's name
 * @return Pointer to the matched command's name struct
 */
static CliCommand* getCommandByName(char* command)
{
    currentCommand = head;

    while (currentCommand != NULL) {
        if (!(strcmp(command, currentCommand->commandName))) {
            return currentCommand;
        }

        currentCommand = currentCommand->next;
    }

    driverSerialWriteString(
        "\n\rIncorrect command, use help to list all the availaible commands\n\r");

    return NULL;
}

/**
 * Converts upper case letters to lower case
 * @param dst output data
 * @param src input data
 * @return none
 */
static void stringToLower(char* dst, const char* src)
{
    while (*src) {
        *dst++ = tolower(*src++);
    }
}

/**
 * Saves the input data from serial that can be a command
 * @param none
 * @return true if user's input was successfully passed to commandbuffer, false if it failed
 */
static bool storeCommand(void)
{
    static uint8_t spaceIndex = 0;  //!< variable storing and index of a space that was found in a string
    static bool spaceChar = false;  //!< flag marking that the space character was found
    static uint8_t counter = 0;     //!< index that is used to traverse through the processed string

    while (driverSerialReadChar(&commandBuffer[counter])) {
        if (commandBuffer[counter] == ' ') {
            spaceIndex = counter;
            afterSpaceLoc = counter + 1;
            spaceChar = true;
        }

        if (commandBuffer[counter] == '\n' || commandBuffer[counter] == '\r') {
            commandBuffer[counter] = '\0';

            if (spaceChar) {
                commandBuffer[spaceIndex] = '\0';
                spaceIndex = 0;
                spaceChar = false;
            }

            counter = 0;

            return true;
        }

        driverSerialWriteChar(commandBuffer[counter]);
        counter++;
    }

    return false;
}

void cliInit(void)
{
    driverSerialInit();
}

void cliProcess(void)
{
    while (storeCommand()) {
        stringToLower(commandBuffer, commandBuffer);
        CliCommand* somecommand = getCommandByName(commandBuffer);

        if (somecommand) {
            if (somecommand->callback) {
                somecommand->callback(&commandBuffer[afterSpaceLoc]);
                commandBuffer[afterSpaceLoc] = '\0';
            }
        }
    }
}

bool cliAddCommand(CliCommand* command)
{
    if (command->callback && command->commandName) {
        if (head == NULL) {
            head = command;
        }
        else {
            currentCommand = head;

            while (currentCommand->next != NULL) {
                currentCommand = currentCommand->next;
            }

            currentCommand->next = command;
        }

        return true;
    }

    return false;
}

void cliPrintAllCommands(void)
{
    driverSerialWriteString("\n\rThese are the commands available to call:\n\r");
    currentCommand = head;

    while (currentCommand != NULL) {
        driverSerialWriteString(currentCommand->commandName);
        driverSerialWriteString(" - ");
        driverSerialWriteString(currentCommand->description);
        driverSerialWriteString("\n\r");
        currentCommand = currentCommand->next;
    }

    driverSerialWriteString("\n\r");
}
