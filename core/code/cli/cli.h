/**
 * \file			cli.h
 * \brief			CLI management layer API
 *
 * \dir             cli
 * \brief           CLI manager
 *
 * \date			Apr 29, 2021
 * \author			Piotr Zmuda - piotr.zmuda.dev@gmail.com
 * \version			v1.0
 */

#pragma once

#include <stdbool.h>
#include <stdint.h>

/**
 * \brief   Function pointer callback used as a handler for executing specified actions
 */
typedef void (*CliCallbackFunc)(char* args);

/**
 * \brief   CLI command data structure that is a single item in a single-conected-list of commands.
 *          Contains a minimal set of information about a command, such as a name or a pointer to an executive function
 */
typedef struct CliCommand {
    CliCallbackFunc callback;  //!< callback to a function
    const char* commandName;   //!< name of a command
    const char* description;   //!< a brief describtion of a command
    struct CliCommand* next;   //!< pointer to the next command
} CliCommand;

/**
 * Initialize serial driver layer
 * @param none
 * @return none
 */
void cliInit(void);

/**
 * Process the commands that are entered by the user and executes them
 * @param none
 * @return none
 */
void cliProcess(void);

/**
 * Adds command to the command list
 * @param command pointer to command struct
 * @return true if command was added successfully, false if it was not.
 *         Improper command name or invalid callback results in false result
 */
bool cliAddCommand(CliCommand* command);

/**
 * Print all defined commands that were added to the commands list
 * @param none
 * @return none
 */
void cliPrintAllCommands(void);
