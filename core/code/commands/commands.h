/**
 * \file			commands.h
 * \brief			CLI command access API
 *
 * \dir             commands
 * \brief           Set of CLI commands occurring in the project
 *
 * \date			Apr 29, 2021
 * \author			Piotr Zmuda - piotr.zmuda.dev@gmail.com
 * \version			v1.0
 */

#pragma once

#include "cli/cli.h"

/**
 * Callback function used to print all available commands
 * @param args not implemented
 * @return none
 */
void helpUser(char* args);

/**
 * Prints time that passed since the user first ran application
 * @param args not implemented
 * @return none
 */
void userTimeFromStart(char* args);

/**
 * Initialises entered gpio in a manner specified by the valid argument and controls the output state or displays the input state
 * @param args parameter that describes in what way and which gpio pin to initialise
 * @return none
 */
void userGpio(char* args);

/**
 * \brief   Initialized data structure for the help command
 */
extern CliCommand CLI_HELP;

/**
 * \brief   Initialized data structure for the time command
 */
extern CliCommand CLI_TIME;

/**
 * \brief   Initialized data structure for the gpio command
 */
extern CliCommand CLI_GPIO;
