/**
 * \file			commands.cpp
 * \brief			Separate file where CLI commands are stored
 *
 * \date			Apr 29, 2021
 * \author			Piotr Zmuda - piotr.zmuda.dev@gmail.com
 * \version			v1.0
 */

#include "commands.h"
#include "FreeRTOS.h"
#include "driverSerial/driverSerial.hpp"
#include "gpioParser/gpioParser.h"
#include "task.h"
#include <stdlib.h>
#include <string.h>

void helpUser(char* args)
{
    cliPrintAllCommands();
}

void userTimeFromStart(char* args)
{
    char tmp[12];

    itoa(xTaskGetTickCount(), tmp, 10);
    driverSerialWriteString("\n\r");
    driverSerialWriteString(tmp);
    driverSerialWriteString("ms\n\r");
}

void userGpio(char* args)
{
    driverSerialWriteString("\n\r");

    GpioParserPin pin = gpioParsePin(args);

    if (GPIO_PARSER_PIN_UNSUPPORTED != pin) {
        char* argument = strchr(args, GPIO_PARSER_SEPARATOR_CHAR) + 1;

        GpioParserAction action = gpioParseAction(argument);

        if (GPIO_PARSER_ACTION_UNSUPPORTED != action) {

            driverSerialWriteString("parsed: ");

            if (action < GPIO_PARSER_ACTION_READ) {
                driverGpioModeSet((DriverGpioPin)pin, (DriverGpioMode)action);
                driverSerialWriteString("mode ");
                driverSerialWriteString(argument);
            }
            else if (action == GPIO_PARSER_ACTION_READ) {
                bool readVal = driverGpioGet((DriverGpioPin)pin);

                driverSerialWriteString("get ");

                if (readVal) {
                    driverSerialWriteString(GPIO_PARSER_PIN_TRUE_VALUE);
                }
                else {
                    driverSerialWriteString(GPIO_PARSER_PIN_FALSE_VALUE);
                }
            }
            else if (action == GPIO_PARSER_ACTION_WRITE_TRUE) {
                driverGpioSet((DriverGpioPin)pin, true);
                driverSerialWriteString("set 1");
            }
            else if (action == GPIO_PARSER_ACTION_WRITE_FALSE) {
                driverGpioSet((DriverGpioPin)pin, false);
                driverSerialWriteString("set 0");
            }
        }
        else {
            driverSerialWriteString("arg not recognized");
        }
    }
    else {
        driverSerialWriteString("arg not recognized");
    }

    driverSerialWriteString("\n\r");
}

CliCommand CLI_HELP = {.callback = helpUser,
                       .commandName = (const char*)"help",
                       .description = (const char*)"List of all available commands"};

CliCommand CLI_TIME = {.callback = userTimeFromStart,
                       .commandName = (const char*)"time",
                       .description = (const char*)"System time in [ms]"};

CliCommand CLI_GPIO = {.callback = userGpio,
                       .commandName = (const char*)"gpio",
                       .description = (const char*)"Change state of the GPIO pin. \n\r Use case: gpio pin.action\n\r List of actions:\n\r  hz - input High Impedance\n\r  pu - input with Pull-Up\n\r  pd - input with Pull-Down\n\r  pp - output as push-Pull\n\r  od - output as Open-Drain\n\r  odpu - output as Open-Drain with Pull-Up\n\r  get - read pin state\n\r  1/0 - set pin state\n\r Example: 'gpio b10.odpu' - GPIOB10 as Open-Drain with Pull-Up"};
